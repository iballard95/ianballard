import {Injectable} from '@angular/core';

import {AngularFireDatabase, AngularFireList} from "angularfire2/database";
import {Certification} from './certification.model';
import {ToastrService} from "ngx-toastr";

@Injectable()
export class CertificationService {

  certificationList: AngularFireList<any>;
  selectedCertification: Certification = new Certification();
  constructor(private firebase : AngularFireDatabase,
              private toastr : ToastrService) { }

  getCertifications(){
    this.certificationList = this.firebase.list('certifications');
    return this.certificationList;
  }

  insertCertification(certification : Certification){
    this.certificationList.push({
      name: certification.name,
      description: certification.description,
      location: certification.location
    }).then(() => {
      this.toastr.success('Inserted Successfully', 'Certification');
    });
  }

  updateCertification(certification : Certification){
    this.certificationList.update(certification.$key,
      {
        name: certification.name,
        description: certification.description,
        location: certification.location
      }).then(() => {
      this.toastr.success('Updated Successfully', 'Certification');
    });
  }

  deleteCertification($key : string){
    this.certificationList.remove($key);
  }

}
