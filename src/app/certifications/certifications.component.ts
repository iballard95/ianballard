import {Component, OnInit} from '@angular/core';
import {CertificationService} from './certification.service'
import {User} from "../providers/user";
import {AuthService} from "../auth/auth.service";
import {NgForm} from "@angular/forms";
import {Certification} from "./certification.model";
import {ToastrService} from "ngx-toastr";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-certifications',
  templateUrl: './certifications.component.html',
  styleUrls: ['./certifications.component.css'],
  providers: [CertificationService, NgbModal]
})
export class CertificationsComponent implements OnInit {

  certificationList : Certification[];
  user : User;

  constructor(private certificationService : CertificationService,
              private auth: AuthService,
              private toastr : ToastrService,
              private modalService: NgbModal) {}

  ngOnInit() {
    this.resetForm();
    this.auth.user$.subscribe(user => this.user = user);
    let x = this.certificationService.getCertifications();
    x.snapshotChanges().subscribe(item => {
      this.certificationList = [];
      item.forEach(element => {
        let y = element.payload.toJSON();
        y["$key"] = element.key;
        this.certificationList.push(y as Certification);
      });
    });
  }

  onSubmit(certificationForm : NgForm){
    //refactor this close event
    document.getElementById("closeBtn").click();
    if(certificationForm.value.$key == null) {
      this.certificationService.insertCertification(certificationForm.value);
    } else {
      this.certificationService.updateCertification(certificationForm.value);
    }
    this.resetForm(certificationForm);
  }

  resetForm(certificationForm? : NgForm){
    if(certificationForm != null) {
      certificationForm.reset();
    }
    this.certificationService.selectedCertification = {
      $key : null,
      name : '',
      description : '',
      location : ''
    }
  }

  onAdd(content) {
    this.resetForm();
    this.open(content);
  }

  onEdit(certification : Certification, content){
    this.open(content);
    this.certificationService.selectedCertification = Object.assign({}, certification);
  }

  onDelete(key : string){
    if (confirm("Are you sure you would like to delete this record?") == true) {
      this.certificationService.deleteCertification(key);
      this.toastr.warning("Deleted Successfully", "Register");
    }
  }

  open(content) {
    this.modalService.open(content);
  }

}
