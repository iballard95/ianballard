export class Certification {
  $key : string;
  name : string;
  description : string;
  location : string;
}
