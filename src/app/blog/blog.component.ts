import {Component, OnInit} from '@angular/core';
import {BlogService} from './blog.service'
import {User} from "../providers/user";
import {AuthService} from "../auth/auth.service";
import {NgForm} from "@angular/forms";
import {Blog} from "./blog.model";
import {ToastrService} from "ngx-toastr";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers: [BlogService, NgbModal]
})
export class BlogComponent implements OnInit {

  blogList : Blog[];
  user : User;

  constructor(private blogService : BlogService,
              private auth: AuthService,
              private toastr : ToastrService,
              private modalService: NgbModal) {}

  ngOnInit() {
    this.resetForm();
    this.auth.user$.subscribe(user => this.user = user);
    let x = this.blogService.getBlogs();
    x.snapshotChanges().subscribe(item => {
      this.blogList = [];
      item.forEach(element => {
        let y = element.payload.toJSON();
        y["$key"] = element.key;
        this.blogList.unshift(y as Blog);
      });
    });
  }

  onSubmit(form : NgForm){
    //refactor this close event
    document.getElementById("closeBtn").click();
    if(form.value.$key == null) {
      this.blogService.insertBlog(form.value);
    }
    this.resetForm(form);
  }

  resetForm(form? : NgForm){
    if(form != null) {
      form.reset();
    }
    this.blogService.selectedBlog = {
      $key : null,
      date : '',
      content : '',
      time : ''
    }
  }

  onAdd(content) {
    this.resetForm();
    this.open(content);
  }

  open(content) {
    this.modalService.open(content);
  }

}
