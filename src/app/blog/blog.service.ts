import {Injectable} from '@angular/core';

import {AngularFireDatabase, AngularFireList} from "angularfire2/database";
import {Blog} from './blog.model';
import {ToastrService} from "ngx-toastr";

@Injectable()
export class BlogService {

  blogList: AngularFireList<any>;
  selectedBlog: Blog = new Blog();
  constructor(private firebase : AngularFireDatabase,
              private toastr : ToastrService) { }

  getBlogs(){
    this.blogList = this.firebase.list('blogs', ref => {
      return ref.orderByChild("/time");
    });
    return this.blogList;
  }

  insertBlog(blog : Blog){
    this.blogList.push({
      date: new Date().toDateString(),
      time: new Date().getTime(),
      content: blog.content
    }).then(() => {
      this.toastr.success('Inserted Successfully', 'Blog');
    });
  }


}
