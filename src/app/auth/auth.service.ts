import {Injectable} from "@angular/core";
import 'rxjs/add/operator/toPromise';
import {AngularFireAuth} from "angularfire2/auth";
import {Observable} from "rxjs";
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import * as firebase from "firebase";
import {User} from "../providers/user";
import {AngularFirestore, AngularFirestoreDocument} from "angularfire2/firestore";
import { ToastrService } from "ngx-toastr";

@Injectable()
export class AuthService {
  public user$: Observable<User>;
  constructor(public afAuth: AngularFireAuth,
              public afs: AngularFirestore,
              private toastr : ToastrService,){
    afs.firestore.settings({ timestampsInSnapshots: true });
    this.user$ = this.afAuth.authState.switchMap(user => {
      if(user){
        return this.afs.doc<User>('users/' + user.uid).valueChanges();
      } else {
        return Observable.of(null);
      }
    });
  }

  doGoogleLogin(){
    let provider = new firebase.auth.GoogleAuthProvider();
    this.afAuth.auth.signInWithPopup(provider)
      .then(credential => {
      this.upsertUser(credential.user);
    });
  }

  upsertUser(user){
    const userRef: AngularFirestoreDocument<any> = this.afs.doc('users/' + user.uid);
    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      roles: {
        subscriber: true
      }
    };
    return userRef.set(data, {merge: true});
  }

  doLogout(){
    return new Promise((resolve, reject) => {
      if(firebase.auth().currentUser){
        this.afAuth.auth.signOut()
          .then(() => {
              this.toastr.success("Logout Successful", "Logout");
            });
        resolve();
      }
      else{
        this.toastr.error("Logout Unsuccessful", "Logout");
        reject();
      }
    });
  }

}
