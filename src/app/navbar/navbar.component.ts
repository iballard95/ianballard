import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  elements: HTMLCollectionOf<Element>;
  visibleElement: HTMLElement;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver) {}

  toggleElement(component : string) {
    this.elements = document.getElementsByClassName('component');
    for (let i = 0; i < this.elements.length; i++) {
      let e = this.elements.item(i) as HTMLElement;
      e.style.display = "none";
    }
    this.visibleElement = document.getElementById(component);
    this.visibleElement.style.display = "block";
  }

  }
