// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCAbE8uZwi5dXQyy6HIK1BB4OuBXUdVgC4",
    authDomain: "angular5fb-88c15.firebaseapp.com",
    databaseURL: "https://angular5fb-88c15.firebaseio.com",
    projectId: "angular5fb-88c15",
    storageBucket: "angular5fb-88c15.appspot.com",
    messagingSenderId: "372420594436"
  }
};
